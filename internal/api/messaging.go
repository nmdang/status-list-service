package api

import (
	"context"
	"encoding/json"
	"github.com/cloudevents/sdk-go/v2/event"
	log "github.com/sirupsen/logrus"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"sync"
)

type creationOffer struct {
	TenantId string `json:"tenantId"`
}

func handleCreate(ctx context.Context, event event.Event) (*event.Event, error) {
	var eventData creationOffer
	if err := json.Unmarshal(event.Data(), &eventData); err != nil {
		log.Error(err)
		return nil, err
	}

	log.Infof("new Event: %v", eventData)

	if err := db.CreateTableForTenantIdIfNotExists(ctx, eventData.TenantId); err != nil {
		log.Error(err)
		return nil, err
	}

	statusData, err := db.AllocateIndexInCurrentBlock(ctx, eventData.TenantId)
	if err != nil {
		log.Error(err)
		return nil, err
	}

	answerData, err := json.Marshal(statusData)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	answerEvent, err := cloudeventprovider.NewEvent("status-list-service", "status.data.v1", answerData)
	if err != nil {
		log.Error(err)
		return nil, err
	}

	return &answerEvent, nil
}

func startMessaging(creationTopic string, group *sync.WaitGroup) {
	defer group.Done()

	client, err := cloudeventprovider.NewClient(cloudeventprovider.Rep, creationTopic)
	if err != nil {
		panic(err)
	}
	defer client.Close()

	err = client.Reply(handleCreate)
	if err != nil {
		panic(err)
	}
}
