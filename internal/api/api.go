package api

import (
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/status-list-service/internal/database"
	"sync"
)

var db *database.Database

func Listen(database *database.Database, port int, creationTopic string) {
	var wg sync.WaitGroup

	db = database

	wg.Add(2)
	go startMessaging(creationTopic, &wg)
	go startRest(port, &wg)

	wg.Wait()
}
